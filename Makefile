build:
	go build ./...

test:
	go test -covermode=count -coverprofile=out ./...

coverage:
	go tool cover -html=out