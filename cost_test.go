package cloudnatix

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestInstancePrice_MinutelyCost(t *testing.T) {
	ok := func(cost, exp float64, per, dur time.Duration) {
		p := InstancePrice{
			cost:     cost,
			duration: per,
		}
		assert.Equal(t, exp, p.MinutelyCost(dur))
	}
	ok(1, 10, time.Minute, time.Minute*10)
	ok(1, 10, time.Minute, time.Second*60*10)
	ok(1, 60, time.Second, time.Minute)
	ok(1, float64(1)/float64(60), time.Hour, time.Minute)
}

func TestInstance_S(t *testing.T) {
	ok := func(state string, exp State) {
		i := Instance{State: state}
		assert.Equal(t, exp, i.S())
	}
	ok("pending", Pending)
	ok(" pending ", Pending)
	ok("Pending", Pending)
	ok("terminated", Terminated)
	ok("Pending", Pending)
	ok("foobar", Unknown)
}

func TestClusterSnapshot_Time(t *testing.T) {
	n := time.Date(2020, 10, 1, 0, 0, 0, 0, time.Local)
	c := ClusterSnapshot{
		UnixTimeStamp: n.Unix(),
	}
	assert.Equal(t, n, c.Time())
}

func TestNewStateMachine(t *testing.T) {
	sm := NewStateMachine()
	assert.NotNil(t, sm)
	assert.IsType(t, new(instanceSM), sm)
	csm := sm.(*instanceSM)
	assert.NotNil(t, csm.transition)
	assert.Len(t, csm.transition, 0)
}

func TestStateMachine(t *testing.T) {
	ok := func(sm StateMachine, from, to State, avail bool, exp map[State]map[State]bool) {
		sm.Transition(from, to, avail)
		csm := sm.(*instanceSM)
		assert.Equal(t, exp, csm.transition)
		assert.Equal(t, avail, sm.IsAvailable(from, to))
	}
	sm := NewStateMachine()
	transitions := make(map[State]map[State]bool)
	transitions[Pending] = map[State]bool{Pending: false}
	ok(sm, Pending, Pending, false, transitions)
	ok(sm, Pending, Pending, false, transitions)
	transitions[Pending][Running] = true
	ok(sm, Pending, Running, true, transitions)
	transitions[Running] = map[State]bool{Running: true}
	ok(sm, Running, Running, true, transitions)
	assert.False(t, sm.IsAvailable(Running, Terminated))
	assert.False(t, sm.IsAvailable(Terminated, Running))
}

func TestHeuristicFunction(t *testing.T) {
	j := HeuristicFunction(func(sm StateMachine, from State, t1 time.Time, to State, t2 time.Time) time.Duration {
		return time.Minute
	})
	var i interface{} = j
	_, ok := i.(Heuristic)
	assert.True(t, ok)
	j = Simple
	i = j
	_, ok = i.(Heuristic)
	assert.True(t, ok)
}

func TestSimple(t *testing.T) {
	sm := NewStateMachine()
	sm.Transition(Running, Running, true)
	now := time.Date(2020, 10, 10, 0, 0, 0, 0, time.Local)
	assert.Equal(t, time.Minute, Simple(sm, Running, now, Running, now.Add(time.Minute)))
	assert.Equal(t, time.Duration(0), Simple(sm, Running, now, Pending, now.Add(time.Minute)))
}

func TestNewEstimator(t *testing.T) {
	sm := NewStateMachine()
	h := HeuristicFunction(Simple)
	e := NewEstimator(sm, h)
	assert.NotNil(t, e)
	assert.IsType(t, new(estimator), e)
	ce := e.(*estimator)
	assert.Equal(t, sm, ce.sm)
	assert.NotNil(t, ce.h)
	assert.NotNil(t, SimpleEstimator(sm))
}

func TestEstimator_Estimate(t *testing.T) {
	p := InstancePrice{
		cost:     1,
		duration: time.Minute,
	}
	now := time.Date(2020, 10, 10, 0, 0, 0, 0, time.Local)
	sm := NewStateMachine()
	sm.Transition(Running, Running, true)
	i1 := &Instance{
		ID:    "a",
		State: "running",
	}
	i2 := &Instance{
		ID:    "b",
		State: "running",
	}
	i3 := &Instance{
		ID:    "c",
		State: "running",
	}
	// snapshot with two running instance
	s1 := &ClusterSnapshot{
		Instances:     []*Instance{i1, i2},
		UnixTimeStamp: now.Unix(),
	}
	// snapshot with a new instance = cost 2
	s2 := &ClusterSnapshot{
		Instances:     []*Instance{i1, i2, i3},
		UnixTimeStamp: (now.Add(time.Minute)).Unix(),
	}

	// snapshot with a no report of i1 & i2 instance = cost 2+4
	s3 := &ClusterSnapshot{
		Instances:     []*Instance{i3},
		UnixTimeStamp: (now.Add(5 * time.Minute)).Unix(),
	}

	// snapshot with a  report of i1 i2 i3 instance = cost 2+4+5+9+9
	s4 := &ClusterSnapshot{
		Instances:     []*Instance{i1, i2, i3},
		UnixTimeStamp: (now.Add(10 * time.Minute)).Unix(),
	}
	var snapshots ClusterSnapshots = []*ClusterSnapshot{s1, s2, s3, s4}
	e := SimpleEstimator(sm)
	// equal to snapshot time window
	cost := e.Estimate(snapshots, now, now.Add(time.Minute), p)
	assert.Equal(t, float64(2), cost)

	// within snapshot time window
	cost = e.Estimate(snapshots, now.Add(-time.Minute), now.Add(2*time.Minute), p)
	assert.Equal(t, float64(2), cost)

	//  future time window
	cost = e.Estimate(snapshots, now.Add(time.Minute), now.Add(time.Hour), p)
	assert.Equal(t, float64(27), cost)

	// past time window
	cost = e.Estimate(snapshots, now.Add(-time.Minute), now, p)
	assert.Equal(t, float64(0), cost)

	// overlapping time window
	cost = e.Estimate(snapshots, now.Add(-time.Minute), now.Add(time.Hour), p)
	assert.Equal(t, float64(29), cost)

	// future time window
	cost = e.Estimate(snapshots, now.Add(time.Hour), now.Add(time.Hour), p)
	assert.Equal(t, float64(0), cost)

	// start after end
	cost = e.Estimate(snapshots, now.Add(time.Hour*2), now.Add(time.Hour), p)
	assert.Equal(t, float64(0), cost)

	// nil snapshots
	cost = e.Estimate(nil, now, now.Add(time.Hour), p)
	assert.Equal(t, float64(0), cost)
}

func TestEstimator_Estimate_SingleSnapShot(t *testing.T) {
	p := InstancePrice{
		cost:     1,
		duration: time.Minute,
	}
	now := time.Date(2020, 10, 10, 0, 0, 0, 0, time.Local)
	sm := NewStateMachine()
	sm.Transition(Running, Running, true)
	i1 := &Instance{
		ID:    "a",
		State: "running",
	}
	i2 := &Instance{
		ID:    "a",
		State: "running",
	}
	s1 := &ClusterSnapshot{
		Instances:     []*Instance{i1, i2},
		UnixTimeStamp: now.Unix(),
	}
	var snapshots ClusterSnapshots = []*ClusterSnapshot{s1}
	e := SimpleEstimator(sm)
	cost := e.Estimate(snapshots, now, now.Add(time.Minute), p)
	assert.Equal(t, float64(0), cost)
}

func TestDefaultStateMachine(t *testing.T) {
	sm := DefaultStateMachine()
	csm := sm.(*instanceSM)
	for from, states := range csm.transition {
		for to, isAvailable := range states {
			assert.Equal(t, from == Running || to == Running, isAvailable)
			assert.Equal(t, isAvailable, sm.IsAvailable(from, to))
		}
	}
}
