// package cloudnatix estimates cost of running cluster
package cloudnatix

import (
	"sort"
	"strings"
	"time"
)

// InstancePrice represent cost to run an instance for some duration
type InstancePrice struct {
	cost     float64
	duration time.Duration
}

// MinutelyCost returns cost to run instance for a given duration
func (p InstancePrice) MinutelyCost(dur time.Duration) float64 {
	return dur.Minutes() * (p.cost / p.duration.Minutes())
}

// State represent status of a computer instance
type State int

const (
	// Running state is when instance is up and ready to execute a program
	Running = iota
	// Terminated state is when instance is terminated
	Terminated
	// Pending state is when instance is being brought up
	Pending
	// Unknown state is un recognized state
	Unknown
)

// Instance represent a state of a thing.
type Instance struct {
	ID    string
	State string
}

// S converts string state representation to type State
func (i Instance) S() State {
	switch strings.ToLower(strings.TrimSpace(i.State)) {
	case "pending":
		return Pending
	case "running":
		return Running
	case "terminated":
		return Terminated
	default:
		return Unknown
	}
}

// ClusterSnapshot represent state of many Instance at a given time.
type ClusterSnapshot struct {
	Instances []*Instance
	// timestamp is the time when the snapshot was taken.
	UnixTimeStamp int64
}

// Time converts unix time stamp to time.Time
func (c ClusterSnapshot) Time() time.Time {
	return time.Unix(c.UnixTimeStamp, 0)
}

// ClusterSnapshots represent multiple cluster snapshots
type ClusterSnapshots []*ClusterSnapshot

// StateMachine describes instance state machine and identifies state transition during which instance was
// available to do execute some task.
type StateMachine interface {
	// Transition add a valid state transition to state machine, isReady indicates if instance is available to
	// to execute some task during this transition.
	Transition(from, to State, isAvailable bool)
	// IsAvailable return true if instance is partially available for executing task during state transition
	IsAvailable(from, to State) bool
}

type instanceSM struct {
	transition map[State]map[State]bool
}

// NewStateMachine return a SateMachine for instance state transition
func NewStateMachine() StateMachine {
	return &instanceSM{transition: make(map[State]map[State]bool)}
}

// DefaultSateMachine returns a state machine which assume that instance is available for workload
// if either initial or final state is Running.
func DefaultStateMachine() StateMachine {
	sm := NewStateMachine()
	sm.Transition(Running, Running, true)
	sm.Transition(Running, Pending, true)
	sm.Transition(Running, Terminated, true)
	sm.Transition(Terminated, Terminated, false)
	sm.Transition(Terminated, Pending, false)
	sm.Transition(Terminated, Running, true)
	sm.Transition(Pending, Pending, false)
	sm.Transition(Pending, Running, true)
	sm.Transition(Pending, Terminated, false)
	return sm
}

func (i *instanceSM) Transition(from, to State, isAvailable bool) {
	if _, ok := i.transition[from]; !ok {
		i.transition[from] = make(map[State]bool)
	}
	i.transition[from][to] = isAvailable
}

func (i *instanceSM) IsAvailable(from, to State) bool {
	if _, ok := i.transition[from]; !ok {
		return ok
	}
	return i.transition[from][to]
}

// Heuristic describes ways to estimate total duration for which an instance is available to execute some task
// between some state transition. A simple heuristic is to assume original state until next observation.
type Heuristic interface {
	// ReadyDuration calculate duration for which a an instance was ready to do some work.
	ReadyDuration(sm StateMachine, from State, t1 time.Time, to State, t2 time.Time) time.Duration
}

// HeuristicFunction is an adapter which allows simple function with valid signature to be used as Heuristic.
type HeuristicFunction func(sm StateMachine, from State, t1 time.Time, to State, t2 time.Time) time.Duration

// ReadyDuration implements Heuristic interface
func (ht HeuristicFunction) ReadyDuration(sm StateMachine, from State, t1 time.Time, to State, t2 time.Time) time.Duration {
	return ht(sm, from, t1, to, t2)
}

// Simple a straight forward heuristic that assumes initial state for complete duration of state transition.
func Simple(sm StateMachine, from State, t1 time.Time, to State, t2 time.Time) time.Duration {
	if !sm.IsAvailable(from, to) {
		return time.Duration(0)
	}
	return t2.Sub(t1)
}

// ClusterCostEstimator estimate cost to run an cluster of instances
type ClusterCostEstimator interface {
	Estimate(snapshots ClusterSnapshots, start, end time.Time, price InstancePrice) float64
}

type observation struct {
	i *Instance
	t *ClusterSnapshot
}

type estimator struct {
	sm StateMachine
	h  Heuristic
}

// NewEstimator returns cluster cost estimator
func NewEstimator(sm StateMachine, h Heuristic) ClusterCostEstimator {
	return &estimator{
		sm: sm,
		h:  h,
	}
}

// SimpleEstimator returns a cluster cost estimator which used Simple heuristic for cost estimation
func SimpleEstimator(sm StateMachine) ClusterCostEstimator {
	return NewEstimator(sm, HeuristicFunction(Simple))
}

// Estimate returns cost of cluster.
func (e *estimator) Estimate(snapshots ClusterSnapshots, start, end time.Time, price InstancePrice) float64 {
	var cost float64
	if !start.Before(end) {
		return cost
	}
	if len(snapshots) == 0 {
		return cost
	}
	// order snapshots in increasing order of time
	sort.Slice(snapshots, func(i, j int) bool {
		return snapshots[i].UnixTimeStamp < snapshots[j].UnixTimeStamp
	})
	previousObservation := make(map[string]observation)
	// process snapshots between start and end time
	for _, snapshot := range snapshots {
		if snapshot.Time().Before(start) || snapshot.Time().After(end) {
			continue
		}
		for _, instance := range snapshot.Instances {
			if prev, ok := previousObservation[instance.ID]; !ok {
				// first time seeing this instance
				previousObservation[instance.ID] = observation{
					i: instance,
					t: snapshot,
				}
			} else {
				// known instance calculate cost
				dur := e.h.ReadyDuration(e.sm, prev.i.S(), prev.t.Time(), instance.S(), snapshot.Time())
				cost += price.MinutelyCost(dur)
				// override past state for this instance
				previousObservation[instance.ID] = observation{
					i: instance,
					t: snapshot,
				}
			}
		}
	}
	return cost
}
