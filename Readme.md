## Cluster Cost Estimator
This program estimates cost of running cluster. To estimate cost you need following: 

- InstancePrice, represent cost to run an instance for some duration
```go
p :=InstancePrice{
    cost:     1,
    duration: time.Minute,
}
```
- State of Instance
```go
i := Instance{
    ID:    "a",
    State: "running",
}
```
- Cluster Snapshot, state of all instances that are make cluster at a given time
```go
cs := ClusterSnapshot{
    Instances:     []*Instance{&i},
    UnixTimeStamp: now.Unix(),
}
```
- You will need more than one snapshot of cluster. 
```go
css := []*ClusterSnapshot{&cs}
```

- `StateMachine` instance state transitions, this represents possible state a instance 
can transition into and if instance is available for workload execution during transition

    A *DefaultStateMachine* that assumes instance is available for workload if either initial or final 
    state is Runing is provided. 
    
    You are welcome to create a more complicated `StateMachine`

```go
sm := NewStateMachine()
sm.Transition(Running, Running, true)
sm.Transition(Pending, Terminated, false)
```

- `Heuristic` describes ways to estimate total duration for which an instance is available to execute some task between some state transition. A simple heuristic is to assume original state until next observation.

    A **Simple** Heuristic that assumes initial state until final state measurement is provide.

    You create more complicated heuristic by implementing Heuristic interface, few example that you can create are:

    - *Mean Time in State*
    - *Half Time in Initial State*

- `ClusterCostEstimator` estimate cost to run cluster using a valid `StateMachine`, `Heuristic` and `ClusterSnapshots`
```go
now := time.Now()
e := SimpleEstimator(sm)
e.Estimate(css, now, now.Add(time.Minute), p)
```